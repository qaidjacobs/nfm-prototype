# Nfield Manager Prototype

This is the complete set of build files representing the Nfield Manager's views as they are designed within the Inspinia design theme.

The design is evolving and ongoing. Make sure you pull locally for the latest version of these interfaces.

## NOTES
The Sass files that compile to the `style.css` document are in the assets/css folder:

```
.
├── assets
│   ├── css
│      ├── *.scss
```

## QUESTIONS?
Contact me: 
- q.jacobs@niposoftware.com
