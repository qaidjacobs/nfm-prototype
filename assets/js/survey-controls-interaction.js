$(function () { 
    // Clicking the publish button
    $('#publish-live-survey').click(function() {
    	$('#publish-live-survey span').text('Published');
    	$('#publish-live-survey').addClass('activated');
    	$('.play-pause-finish').removeClass('hide');
    })

    // Clicking the play button
    $('a.live-survey-play').click(function() {
        $('.ibox-title h2 span').text('has been started').addClass('status-started').removeClass('status-paused');
        $('a.live-survey-play').addClass('disabled');
    	$('a.live-survey-pause').removeClass('disabled');
        $('.ibox-title h2 i').removeClass('fa-pause m-r-sm default-color').addClass('fa-play m-r-sm new-item-color');
    })

    // Clicking the pause button
    $('a.live-survey-pause').click(function() {
        $('.ibox-title h2 span').text('has been paused').addClass('status-paused').removeClass('status-started');
        $('a.live-survey-pause').addClass('disabled');
        $('a.live-survey-play').removeClass('disabled');
        $('a.live-survey-finish').removeClass('disabled');
        $('.ibox-title h2 i').removeClass('fa-play m-r-sm new-item-color').addClass('fa-pause m-r-sm default-color');
    })

    // Clicking the finish button
    $('a.live-survey-finish').click(function() {
        $('.ibox-title h2 i').removeClass('fa-pause m-r-sm default-color').addClass('fa-check m-r-sm least-impt-color');
        $('.ibox-title h2 span').text('is finished').addClass('status-finished');
        $('#publish-live-survey').text('Finished').addClass('disabled');
        $('a.live-survey-pause, a.live-survey-play, a.live-survey-finish').addClass('disabled');    

        
    })
});